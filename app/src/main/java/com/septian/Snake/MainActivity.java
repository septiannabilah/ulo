package com.septian.Snake;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.rtp.AudioStream;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.games.Games;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity implements  View.OnClickListener {


    private static final String TAG = "MainActivity";
    ImageButton play, leaderboard, achiev, credit;

    final public int BUTTON_SIGN_IN = 2;
    final public int BUTTON_SIGN_OUT = 1;

    public final static int RC_SIGN_IN = 9001;
    public static final int RC_LEADERBOARD_UI = 9004;
    public static final int RC_ACHIEVEMENT_UI = 9003;

    private Context context;
    private ServiceManager serviceManager;

    Button signOutButton;

    private SharedPreferences load;
    private SharedPreferences save;
    private SharedPreferences highScore;
    private SharedPreferences.Editor saveEditor;
//    private boolean isAuthenticated;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);

        context = this;
        serviceManager = new ServiceManager(this);

        save = PreferenceManager.getDefaultSharedPreferences(context);
        saveEditor = save.edit();

        load = PreferenceManager.getDefaultSharedPreferences(context);
//        isAuthenticated = load.getBoolean("Authenticated", true);

        play = findViewById(R.id.play);
        leaderboard = findViewById(R.id.leaderboard);
        achiev = findViewById(R.id.achiev);
        credit = findViewById(R.id.credit);
        signOutButton = findViewById(R.id.signout);

        signOutButton.setId(BUTTON_SIGN_OUT);
        signOutButton.setOnClickListener(this);


        updateUI(null);

        play.setOnClickListener(this);
        achiev.setOnClickListener(this);
        leaderboard.setOnClickListener(this);
        credit.setOnClickListener(this);

        serviceManager.signIn();
//        updateUI(null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case BUTTON_SIGN_OUT:
                updateUI(null);
                Toast.makeText(this, "Log out", Toast.LENGTH_SHORT).show();
                break;
            case R.id.play:
                startActivity(new Intent(MainActivity.this, SnakeActivity.class));
                break;
            case R.id.achiev:
                serviceManager.showAchievement();
                break;
            case R.id.leaderboard:
                serviceManager.showLeaderboard();
                break;
            case R.id.credit:
                startActivity(new Intent (MainActivity.this, Credit.class));
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            updateUI(account);
        } catch (ApiException e) {
            showToast("Error Code: " + e.getStatusCode());
        }
        updateUI(null);
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {
//        signOutButton.setVisibility(isSignedIn() ? View.GONE : View.VISIBLE);
//        saveEditor.putBoolean("Authenticated", isAuthenticated);
//        saveEditor.commit();
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
