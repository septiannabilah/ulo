package com.septian.Snake;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

public class SoundPlayer {
    private AudioAttributes audioAttributes;
    final int SOUND_POOL_MAX = 3;

    private static SoundPool soundPool;
    private static int eatFruit;
    private static int bgm;

    public SoundPlayer(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .setMaxStreams(SOUND_POOL_MAX)
                    .build();
        } else {
            soundPool = new SoundPool(SOUND_POOL_MAX, AudioManager.STREAM_MUSIC, 0);
        }

        eatFruit = soundPool.load(context, R.raw.sfx, 1);
        bgm = soundPool.load(context, R.raw.bgm, 1);
    }

    public void playEatFruitSound(){
        soundPool.play(eatFruit, 1.0f, 1.0f, 1,0, 1.0f);
    }

    public void playBGM(){
        soundPool.play(bgm, 1.0f, 1.0f, 1,0, 1.0f);
    }
}
