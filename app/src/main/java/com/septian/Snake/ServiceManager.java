package com.septian.Snake;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Debug;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.games.Games;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class ServiceManager implements PlayInterface {

    private Context context;
    private GoogleSignInClient googleSignInClient;
    private GoogleSignInAccount googleSignInAccount;
    private GoogleSignInOptions googleSignInOptions;

    public ServiceManager(Context context) {
        this.context = context;

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(context, gso);
    }

    @Override
    public void signIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        ((AppCompatActivity) context).startActivityForResult(signInIntent,
                MainActivity.RC_SIGN_IN);
    }

    @Override
    public void signOut() {
        googleSignInClient.revokeAccess();
    }

    @Override
    public boolean isSignedIn() {
        googleSignInAccount = GoogleSignIn.getLastSignedInAccount(context);
        return googleSignInAccount != null;
    }

    @Override
    public void showLeaderboard() {
        if (!isSignedIn()) return;

        Games.getLeaderboardsClient(context, GoogleSignIn.getLastSignedInAccount(context))
                .getLeaderboardIntent(context.getString(R.string.leaderboard))
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        ((AppCompatActivity) context).startActivityForResult(intent,
                                MainActivity.RC_LEADERBOARD_UI);
                    }
                });
    }

    @Override
    public void submitScore(int score) {
        if (!isSignedIn()) return;

        Games.getLeaderboardsClient(context, GoogleSignIn.getLastSignedInAccount(context))
                .submitScore(context.getString(R.string.leaderboard), score);
    }

    @Override
    public void showAchievement() {
        if (!isSignedIn()) return;

        Games.getAchievementsClient(context, GoogleSignIn.getLastSignedInAccount(context))
                .getAchievementsIntent()
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        ((AppCompatActivity) context).startActivityForResult(intent,
                                MainActivity.RC_ACHIEVEMENT_UI);
                    }
                });
    }

    @Override
    public void unlockAchievement(int index) {
        if (!isSignedIn()) return;

        Log.d("Service", "Kontol");

        @SuppressLint("Recycle")
        TypedArray achiev = context.getResources().obtainTypedArray(R.array.achievements);

        Games.getAchievementsClient(context, GoogleSignIn.getLastSignedInAccount(context))
                .unlock(achiev.getString(index));
    }
}
