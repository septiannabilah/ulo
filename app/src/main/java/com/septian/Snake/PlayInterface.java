package com.septian.Snake;

public interface PlayInterface {
    void signIn();
    void signOut();
    boolean isSignedIn();
    void showLeaderboard();
    void submitScore(int score);
    void showAchievement();
    void unlockAchievement(int index);
}
