package com.septian.Snake;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.IOException;
import java.util.Random;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class SnakeEngine extends SurfaceView implements Runnable {

    // game thread for the main game loop
    private Thread thread = null;

    // hold a reference to the activity
    private Context context;

    // sound fx
    private SoundPool soundPool;
    private int eat_bob = -1;
    private int snake_crash = -1;

    // tracking movement heading
    public enum Heading {UP, DOWN, LEFT, RIGHT};
    // starting heading movement
    private Heading heading = Heading.RIGHT;

    // screen size
    private int screen_X;
    private int screen_y;

    // snake length
    private int snakeLength;

    // Where is Bob hiding?
    private int bobX;
    private int bobY;

    // snake segment's size in pixels
    private int blockSize;

    // size in segments of the playable area
    private final int NUM_BLOCKS_WIDE = 20;
    private int numBlocksHigh;

    // control pausing between update;
    private long nextFrameTime;
    // update the game 10 times per second
    private final long FPS = 5;
    // there are 1000 ms in a second
    private final long MILLIS_PER_SECOND = 1000;
    // We will draw the frame much more often

    // how many points does the player have
    private int score;

    // the location in the grid of all the segments
    private int[] snakeXs;
    private int[] snakeYs;

    // everything we need for drawing
    // is the game currently playing ?
    private volatile boolean isPlaying;

    // canvas for paint
    private Canvas canvas;

    // required to use canvas
    private SurfaceHolder surfaceHolder;

    // some paint for canvas
    private Paint paint;

    private ServiceManager serviceManager;

    InterstitialAd ads;

    Bitmap bitmapSnake;
    Bitmap bitmapSnakeBody;
    Bitmap bitmapSnakeTail;

    Matrix matrixSnake;
    Matrix matrixSnakeBody;
    Matrix matrixSnakeTail;

    Bitmap bitmapFruit;

//    SoundPlayer soundPlayer;

    public SnakeEngine(Context context, Point size){
        super(context);

        this.context = context;
        serviceManager = new ServiceManager(context);

        //Interstitial
        ads = new InterstitialAd(context);
        ads.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        ads.loadAd(new AdRequest.Builder().build());
        ads.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.d("MainActivity", "onAdClosed");
            }
        });

        screen_X = size.x;
        screen_y = size.y;

        // pixels in each block
        blockSize = screen_X / NUM_BLOCKS_WIDE;
        // bloks that will fit into the height
        numBlocksHigh = screen_y / blockSize;

        // set the sound
//        soundPlayer = new SoundPlayer(this.context);

        // initialize the drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();

        // if score 200 will crash achievment
        snakeXs = new int[200];
        snakeYs = new int[200];

        // start game
        newGame();
    }

    public void showAds()
    {
        Log.d("MainActivity", "showInterstitial");

        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (ads.isLoaded())
                    ads.show();
            }
        });
    }

    @Override
    public void run(){
        while (isPlaying){
            // update 10 times/second
            if(updateRequired()){
                update();
                draw();
            }
        }
    }

    public void pause(){
        isPlaying = false;
        try {
            thread.join();
        } catch (InterruptedException e){
            // error
        }
    }

    public void resume(){
        isPlaying = true;
        thread = new Thread(this);
        thread.start();
    }

    public void newGame(){
        snakeLength = 3;
        snakeXs[0] = NUM_BLOCKS_WIDE / 2;
        snakeYs[0] = numBlocksHigh / 2;

        spawnBob();

        score = 0;

        nextFrameTime = System.currentTimeMillis();
    }

    public void spawnBob(){
        Random random = new Random();
        bobX = random.nextInt(NUM_BLOCKS_WIDE - 1) + 1;
        bobY = random.nextInt(numBlocksHigh - 2) + 1;
    }

    public void eatBob(){
//        soundPlayer.playEatFruitSound();
        snakeLength++;

        score++;
        if (score >= 1) {
            serviceManager.unlockAchievement(0);
        }

        spawnBob();
    }

    private void moveSnake(){
        // move the body
        for (int i = snakeLength; i > 0; i--){
            snakeXs[i] = snakeXs[i - 1];
            snakeYs[i] = snakeYs[i - 1];

        }

        matrixSnake = new Matrix();
        matrixSnakeBody = new Matrix();
        matrixSnakeTail = new Matrix();
        // move the head in the direction
        switch (heading){
            case UP:
                matrixSnake.postRotate(0);
                matrixSnakeBody.postRotate(90);
                matrixSnakeTail.postRotate(0);
                snakeYs[0]--;
                break;
            case DOWN:
                matrixSnake.postRotate(180);
                matrixSnakeBody.postRotate(90);
                matrixSnakeTail.postRotate(180);
                snakeYs[0]++;
                break;
            case LEFT:
                matrixSnake.postRotate(270);
                matrixSnakeBody.postRotate(0);
                matrixSnakeTail.postRotate(270);
                snakeXs[0]--;
                break;
            case RIGHT:
                matrixSnake.postRotate(90);
                matrixSnakeBody.postRotate(0);
                matrixSnakeTail.postRotate(90);
                snakeXs[0]++;
                break;
        }
    }

    private boolean detectDeath(){
        boolean dead = false;

        // hit the screen edge
        if (snakeXs[0] == -1)
            dead = true;
        if (snakeXs[0] >= NUM_BLOCKS_WIDE)
            dead = true;
        if (snakeYs[0] == -1)
            dead = true;
        if (snakeYs[0] >= numBlocksHigh - 1)
            dead = true;

        // eaten himself
        for (int i = snakeLength - 1; i > 0; i--){
            if (i > 2 && snakeXs[0] == snakeXs[i] && snakeYs[0] == snakeYs[i])
                dead = true;
        }

        return dead;
    }

    public void update(){
        // head collision with bob
        if (snakeXs[0] == bobX && snakeYs[0] == bobY)
            eatBob();

        moveSnake();

        if (detectDeath()){
//            soundPool.play(snake_crash, 1, 1, 0, 0,1);
            showAds();
            //newGame();
            serviceManager.submitScore(score);
            context.startActivity(new Intent(context, MainActivity.class));

        }
    }

    public void draw(){
        // get a lock on the canvas
        if (surfaceHolder.getSurface().isValid()){
            canvas = surfaceHolder.lockCanvas();


            Bitmap background = BitmapFactory.decodeResource(getResources(), R.drawable.bgnew);

            float scale = (float)background.getWidth() / (float)getWidth();
            int newWidth = Math.round(background.getWidth() / scale);
            int newHeight = Math.round(background.getHeight() / scale);
            Bitmap scaled = Bitmap.createScaledBitmap(background, newWidth, newHeight, true);
            canvas.drawBitmap(scaled, 0, 0, null);

            bitmapSnake = BitmapFactory.decodeResource(this.getResources(), R.drawable.snakehead);
            Bitmap snake = Bitmap.createScaledBitmap(bitmapSnake, 50, 75, true);
            Bitmap rotatedSnake = Bitmap.createBitmap(snake, 0, 0, snake.getWidth(), snake.getHeight(), matrixSnake, true);

            bitmapSnakeBody = BitmapFactory.decodeResource(this.getResources(), R.drawable.snakebody);
            Bitmap snakeBody = Bitmap.createScaledBitmap(bitmapSnakeBody, 50, 50, true);
            Bitmap rotatedSnakeBody = Bitmap.createBitmap(snakeBody, 0, 0, snakeBody.getWidth(), snakeBody.getHeight(), matrixSnakeBody, true);

            bitmapSnakeTail = BitmapFactory.decodeResource(this.getResources(), R.drawable.snaketail);
            Bitmap snakeTail = Bitmap.createScaledBitmap(bitmapSnakeTail, 50, 75, true);
            Bitmap rotatedSnakeTail = Bitmap.createBitmap(snakeTail, 0, 0, snakeTail.getWidth(), snakeTail.getHeight(), matrixSnakeTail, true);


            bitmapFruit = BitmapFactory.decodeResource(this.getResources(), R.drawable.fruit);
            Bitmap fruit = Bitmap.createScaledBitmap(bitmapFruit, 50, 50, true);
            canvas.drawBitmap(fruit, bobX * blockSize, bobY * blockSize, null);

            //Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            // snake white color
            paint.setColor(Color.argb(255, 255,255, 255));

            // scale the HUD text
            paint.setTextSize(90);
            canvas.drawText("Score : " + score, screen_X / 3, screen_y / 13, paint);

            // draw the snake one block at a time
            for (int i = 0; i < snakeLength; i++){
//                canvas.drawRect(snakeXs[i] * blockSize,
//                        (snakeYs[i] * blockSize),
//                        (snakeXs[i] * blockSize) + blockSize,
//                        (snakeYs[i] * blockSize) + blockSize,
//                        paint);
                if (i == 0){
                    canvas.drawBitmap(rotatedSnake, snakeXs[i] * blockSize, snakeYs[i] * blockSize, null);
                } else if (i > 0 && i < snakeLength - 1){
                    canvas.drawBitmap(rotatedSnakeBody, snakeXs[i] * blockSize, snakeYs[i] * blockSize, null);
                } else if (i == snakeLength - 1){
                    matrixSnakeTail.postRotate(0);
                    canvas.drawBitmap(rotatedSnakeTail, snakeXs[i] * blockSize, snakeYs[i] * blockSize, null);
                }
            }

            // set the color of the paint to draw Bob (red)
            paint.setColor(Color.argb(255,255,0,0));

             //draw Bob
//            canvas.drawRect(bobX * blockSize,
//                    (bobY * blockSize),
//                    (bobX * blockSize) + blockSize,
//                    (bobY * blockSize) + blockSize,
//                    paint);

            // unlock the canvas and reveal the
            // graphics for this frame
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    public boolean updateRequired(){
        // update frame
        if (nextFrameTime <= System.currentTimeMillis()){
            // tenth of a second has

            // setup when the next update will be triggered
            nextFrameTime = System.currentTimeMillis() + MILLIS_PER_SECOND / FPS;

            return true;
        }

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent){
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK){
            case MotionEvent.ACTION_UP:
                if (motionEvent.getX() >= screen_X / 2){
                    switch (heading){
                        case UP:
                            heading = Heading.RIGHT;
                            break;
                        case RIGHT:
                            heading = Heading.DOWN;
                            break;
                        case DOWN:
                            heading = Heading.LEFT;
                            break;
                        case LEFT:
                            heading = Heading.UP;
                            break;
                    }
                } else {
                    switch (heading){
                        case UP:
                            heading = Heading.LEFT;
                            break;
                        case LEFT:
                            heading = Heading.DOWN;
                            break;
                        case DOWN:
                            heading = Heading.RIGHT;
                            break;
                        case RIGHT:
                            heading = Heading.UP;
                            break;
                    }
                }
        }
        return true;
    }
}


